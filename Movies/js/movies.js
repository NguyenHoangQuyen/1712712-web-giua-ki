const api = 'https://api.themoviedb.org/3/'
const myAPI_key = 'api_key=282c2cc3ba70fd27d61018ff451e1d76&language=en-US'
const imgPath = 'https://image.tmdb.org/t/p/original'
const less = 100;


$(document).ready(function() {
    $("#searchFormMV").on('submit', evtSubmitName);
    $("#searchFormCast").on('submit', evtSubmitCast);
    getTopRatedMovies(1);

});
async function getTopRatedMovies(page) {

    const request = `${api}movie/top_rated?${myAPI_key}&page=${page}`;
    spinnerLoading();
    const response = await fetch(request);
    const listMovies = await response.json(); // cái trả về là kiểu json
    appendTextOfContent('<div class="col-md-12"><h2>Top Rated</h2></div>');
    fillMovies(listMovies.results);
    appendPagination();
    pageFooterTopRated(listMovies.total_pages, page);
}

function appendPagination() {
    $("#myPagination").empty();
    $('#content').after(` <div id="myPagination" class="row justify-content-center">
    <div id="pager" class="d-flex justify-content-center">
        <ul class="pagination">
            <li id="first" class="page-item">
                <a class="page-link" aria-label="Previous">
                    <span aria-hidden="true">&laquo;</span>
                    <span class="sr-only">Previous</span>
                </a>
            </li>
            <li id="last" class="page-item">
                <a class="page-link" aria-label="Next">
                    <span aria-hidden="true">&raquo;</span>
                    <span class="sr-only">Next</span>
                </a>
            </li>
        </ul>
    </div>
</div>`);
}

function pageFooterTopRated(soTrang, page) {
    var flag = 1;
    if ($('ul #first').next()[0] != $('ul #last')[0]) { //có giá trị thì xóa
        flag = 0;
    }
    if (soTrang > 10) soTrang = 10;
    for (var i = 1; i <= soTrang; i++) {
        if (flag == 0) {
            $('ul #first').next().remove();
        }
        if (page == i) {
            $('ul #last').before(
                `<li class='page-item active' onclick="getTopRatedMovies(${i})"><a class='page-link'>${i}</a></li>`
            );
        } else {
            $('ul #last').before(
                `<li class='page-item' onclick="getTopRatedMovies(${i})"><a class='page-link'>${i}</a></li>`
            );
        }
    }
}

function pageFooterMoviesByName(soTrang, page) {
    if (page == undefined) page = 1;
    var flag = 1;
    if ($('ul #first').next()[0] != $('ul #last')[0]) { //có giá trị thì xóa
        flag = 0;
    }
    if (soTrang > 10) soTrang = 10;
    for (var i = 1; i <= soTrang; i++) {
        if (flag == 0) {
            $('ul #first').next().remove();
        }
        if (page == i) {
            $('ul #last').before(
                `<li class='page-item active' onclick="getMoviesByName(${i})"><a class='page-link'>${i}</a></li>`
            );
        } else {
            $('ul #last').before(
                `<li class='page-item' onclick="getMoviesByName(${i})"><a class='page-link'>${i}</a></li>`
            );
        }
    }
}

function pageFooterMoviesByCast(soTrang, page) {
    if (page == undefined) page = 1;
    var flag = 1;
    if ($('ul #first').next()[0] != $('ul #last')[0]) { //có giá trị thì xóa
        flag = 0;
    }
    if (soTrang > 10) soTrang = 10;
    for (var i = 1; i <= soTrang; i++) {
        if (flag == 0) {
            $('ul #first').next().remove();
        }
        if (page == i) {
            $('ul #last').before(
                `<li class='page-item active' onclick="getMoviesByCast(${i})"><a class='page-link'>${i}</a></li>`
            );
        } else {
            $('ul #last').before(
                `<li class='page-item' onclick="getMoviesByCast(${i})"><a class='page-link'>${i}</a></li>`
            );
        }
    }
}
async function getMoviesByCast(page) {
    const strInputSearch = $('#searchFormCast input').val();
    const request = `${api}search/person?${myAPI_key}&query=${strInputSearch}&page=${page}`;

    spinnerLoading();
    const response = await fetch(request);
    knownFor = await response.json(); // cái trả về là kiểu json
    var listMovies = [];
    for (const result of knownFor.results) {
        if (result.known_for_department === "Acting") {
            for (const film of result.known_for) {

                if (film.original_title === undefined) {
                    //đó là các chương trình truyền hình
                } else {
                    listMovies.push(film);
                }
            }
        }
    }
    $("#content").empty();
    fillMovies(listMovies);
    appendPagination();
    pageFooterMoviesByCast(knownFor.total_pages, page);
}

function appendTextOfContent(text) {
    $("#content").empty();
    $("#content").append(`${text}`);
}

async function getMoviesByName(page) {
    const strInputSearch = $('#searchFormMV input').val();
    const request = `${api}search/movie?${myAPI_key}&query=${strInputSearch}&page=${page}`;
    spinnerLoading();
    const response = await fetch(request);
    const listMovies = await response.json(); // cái trả về là kiểu json
    $("#content").empty();
    fillMovies(listMovies.results);
    appendPagination();
    pageFooterMoviesByName(listMovies.total_pages, page);
}
async function evtSubmitName(e, page) {
    e.preventDefault();
    getMoviesByName(page);

}
async function evtSubmitCast(e, page) {
    e.preventDefault();
    getMoviesByCast(page);
}

function getMoreOverview(movie) {
    var overviewStr = `${movie.overview}`;
    var more = ".....";
    var readLessOverview = overviewStr.substring(0, less) + more;
    if (overviewStr.length <= less) {
        readLessOverview = overviewStr;
    }
    return readLessOverview;
}

function fillMovies(listMovies) {
    for (const movie of listMovies) {
        const imgURL = `${imgPath}${movie.poster_path}`;

        var readLessOverview = getMoreOverview(movie);

        $("#content").append(`
        <div class="col-md-4 border-0 py-1">
        <div class="card card text-white bg-dark mb-3 shadow h-100" onclick="getMovieDetails(${movie.id})">
            <img src="${imgURL}" class="card-img-top" alt="${movie.title} Poster">
            <div class="card-body">
                <h5 class="card-title">${movie.title}</h5>
                <p class="card-text">${readLessOverview}</p>
                <a onclick="getMovieDetails(${movie.id})" class="nav-link btn btn-info">More Details</a>
            </div>
        </div>
    </div>
        `)
    }
}

function spinnerLoading() {
    $("#content").empty();
    $("#content").append(`<div class="height"></div>`)
    $("#myPagination").empty();
    $("#content").append(
        `<div class="full-width-div text-center">
        <div class="spinner-grow text-info" style="width: 3rem; height: 3rem;" role="status">
        <br><br><br>
        </div>
        <h2 class="text-info">Loading...</h2>
      </div>`
    )
}

async function getMovieDetails(mvId) {
    $('#searchFormCast input').val('');
    $("#content").empty();
    spinnerLoading();
    const request = `${api}movie/${mvId}?${myAPI_key}`;
    const response = await fetch(request);
    const details = await response.json(); // cái trả về là kiểu json

    //lay all dien vien
    const request2 = `${api}movie/${mvId}/credits?${myAPI_key}`;
    const response2 = await fetch(request2);
    credits = await response2.json(); // cái trả về là kiểu json

    //director
    crews = credits.crew;
    //for trong crew, tìm job là director
    for (const crew of crews) {
        if (crew.job === "Director") {
            director = crew.name;
        }

    }

    //overview
    const request3 = `${api}movie/${mvId}/reviews?${myAPI_key}`;
    const response3 = await fetch(request3);
    reviews = await response3.json(); // cái trả về là kiểu json

    const imgURL = `${imgPath}${details.poster_path}`;

    $("#content").empty();
    $("#content").append(
        `
        <div class="container py-2">
        <div class="row content">
            <div class="col-sm-4 bg-dark text-white py-3">
                <img id="posterDetails" src="${imgURL}" class="card-img-top">
                <hr>
                <h3>${details.title.toUpperCase()}</h3>
                <hr>
                <br>
                <h5><span class="glyphicon glyphicon-time"></span>Release on ${details.release_date}</h5>
                <h2><small>Overview</small></h2>
                <p>${details.overview}</p>
                <br>
                <h2><small>Genre</small></h2>
                <ul id="genre" class="list-group">
                  
                </ul>
                <br><br>
                <h5><span class="label label-success">${director}</span></h5>
                <p>Director</p>
                

            </div>

            <div class="col-sm-8 px-4 py-3 bg-secondary text-white">
                <h1>${details.title}</h1>
                <hr>
                <h2>Cast</h2>
                <!--append all cast-->
                <div class="container text-light bg-dark">
                    <div id="allCast" class="row">
                    </div>
                </div>
                <br>
                <h4>Leave a Comment:</h4>
                <form role="form">
                    <div class="form-group">
                        <textarea id="sunmitReview" class="form-control" rows="3" required></textarea>
                    </div>
                    <buttonn class="btn btn-success" onclick="SubmitReview()">Submit</buttonn>
                </form>
                <br><br>

                <h4 class="label label-primary text-primary">${reviews.total_results} Reviews</h4><br>

                <div id="allReview" class="row">
                    </div>
            </div>
        </div>
    </div>
    <button class="btn btn-primary ml-3 px-4 m-4" onclick="getTopRatedMovies(1)">GO BACK</button>`
    );
    fillGenre(details);
    fillAllCast(credits);
    fillReview(reviews);
}

function SubmitReview() {

    $("#sunmitReview").val = "";
    window.alert("Thank you. Submit Successfully !!!");
}

function makeImgURL(path) {
    const imgURL = `${imgPath}${path}`;
    return imgURL;
}

function fillAllCast(credits) {
    console.log(credits.id);
    for (const cast of credits.cast) {
        var avt = makeImgURL(cast.profile_path);
        if (cast.profile_path === null) {
            avt = `https://cdn.pixabay.com/photo/2016/08/08/09/17/avatar-1577909_960_720.png`;
        }
        $("#allCast").append(
            `<div class="col-2 col-sm-5 col-lg-3 py-2 d-flex justify-content-between align-items-center" 
            onclick="getCastDetails(${cast.id}, ${credits.id})">
    <div class="image-parent">
        <img src="${avt}" class="img-fluid px-md-2" alt="${cast.name}">
    </div>
    <h5><small>${cast.name}</small></h5>
</div>`
        );
    }
}

function fillGenre(details) {
    for (const genre of details.genres) {
        $("#genre").append(`<li class="list-group-item  bg-dark">${genre.name}</li>`);
    }
}

function fillReview(reviews) {
    for (const review of reviews.results) {
        $("#allReview").append(
            `<div class="col-sm-12 px-3">
          <h4>By ${review.author}</h4>
          <div class="shadow p-4 mb-4">
          <p class="text-justify"><small>${review.content}</small></p>
          </div>
      </div>`
        );
    }
}

async function getCastDetails(cast_id, mvId) {
    const request = `${api}person/${cast_id}?${myAPI_key}`;
    const response = await fetch(request);
    castDetails = await response.json(); // cái trả về là kiểu json
    avt = makeImgURL(castDetails.profile_path);

    $("#content").empty();
    spinnerLoading();
    $("#content").empty();
    $("#content").append(`
    <button class="btn btn-primary ml-3 px-4" onclick="getMovieDetails(${mvId})">GO BACK</button>
    <div class="container">
        <header class="jumbotron my-4">
            <div class="container">
                <div class="row">
                    <div class="col-3 d-flex justify-content-center h-25">
                        <img src="${avt}" class="img-fluid" alt="${castDetails.name}">
                    </div>
                    <div class="col-9">
                        <h1 class="display-3">${castDetails.name}</h1>
                        <h3>Biography</h3>
                        <p class="text-justify">${castDetails.biography}</a>
                    </div>
                </div>
            </div>
        </header>
        <!-- Page Features -->
        <h2 class="text-white">Known For</h2>
        <div id="actorFilm" class="row text-center">
        
        </div>
    </div>
`);
    getKnownFor(castDetails.name);

}

async function getKnownFor(name) {
    const request = `${api}search/person?${myAPI_key}&query=${name}&page=1`;
    const response = await fetch(request);
    knownFor = await response.json(); // cái trả về là kiểu json
    for (const result of knownFor.results) {
        if (result.known_for_department === "Acting") {
            console.log(result.known_for);
            for (const film of result.known_for) {
                if (film.original_title === undefined) {
                    //đó là các chương trình truyền hình
                } else {
                    var imgurl = makeImgURL(film.poster_path);
                    var readLessOverview = getMoreOverview(film);
                    $("#actorFilm").append(`
                    <div class="col-lg-3 col-md-6 mb-4">
                    <div class="card h-100 bg-secondary text-white" onclick="getMovieDetails(${film.id})">
                        <img class="card-img-top" src="${imgurl}" alt="${film.poster_path} poster">
                        <div class="card-body px-2 py-2">
                            <h4 class="card-title">${film.title}</h4>
                            <p class="card-text"><small>${readLessOverview}</small></p>
                            <h6>Vote count</h6>
                            <p>${film.vote_count}</p>
                        </div>
                        <div class="card-footer">
                            <a class="btn btn-primary">More Details</a>
                        </div>
                    </div>
                </div>`)
                }
            }
        }
    }
}